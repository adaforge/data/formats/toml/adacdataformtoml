# use gmake

.PHONY: all build test run install clean clean-all build-development build-validation build-release build-tests

all: build test

build: build-development build-validation build-release

build-development: | build/${CPUTYPE}_${OSTYPE}/development
build/${CPUTYPE}_${OSTYPE}/development:
	alr build --development
	gprbuild -XPROFILE=development -XLIBRARY_TYPE=static
	gprbuild -XPROFILE=development -XLIBRARY_TYPE=relocatable

build-validation: | build/${CPUTYPE}_${OSTYPE}/validation
build/${CPUTYPE}_${OSTYPE}/validation:
	alr build --validation
	gprbuild -XPROFILE=validation -XLIBRARY_TYPE=static
	gprbuild -XPROFILE=validation -XLIBRARY_TYPE=relocatable

build-release: | build/${CPUTYPE}_${OSTYPE}/release
build/${CPUTYPE}_${OSTYPE}/release:
	alr build --release
	gprbuild -XPROFILE=release -XLIBRARY_TYPE=static
	gprbuild -XPROFILE=release -XLIBRARY_TYPE=relocatable

build-tests: build-release build/${CPUTYPE}_${OSTYPE}/development/bin/testsuite
build/${CPUTYPE}_${OSTYPE}/development/bin/testsuite:
	cd tests; alr build

test: build-tests
	@build/${CPUTYPE}_${OSTYPE}/development/bin/testsuite

bin/testsuite: build-tests
	@rm bin; ln -sv build/${CPUTYPE}_${OSTYPE}/development/bin bin

run: bin/testsuite
	@bin/testsuite

install:

clean:
	gprclean -XPROFILE=development -XLIBRARY_TYPE=static
	gprclean -XPROFILE=development -XLIBRARY_TYPE=relocatable
	gprclean -XPROFILE=validation -XLIBRARY_TYPE=static
	gprclean -XPROFILE=validation -XLIBRARY_TYPE=relocatable
	gprclean -XPROFILE=release -XLIBRARY_TYPE=static
	gprclean -XPROFILE=release -XLIBRARY_TYPE=relocatable

clean-all:
	rm -r build
