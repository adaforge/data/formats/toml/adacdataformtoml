--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: BSD-3-Clause
--  SPDX-Creator: Pierre-Marie de Rodat (derodat@adacore.com)
--  SPDX-FileCopyrightText: Copyright 2019 AdaCore (derodat@adacore.com)
--  SPDX-FileContributor: 2019 - 2022 Pierre-Marie de Rodat (pmderodat@kawie.fr)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2019-03-25
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Ada.Strings.Unbounded;
with Ada.Text_IO;

with TOML;
with TOML.File_IO;

procedure Test_Iterate_on_Table is
   package TIO renames Ada.Text_IO;

   Value : constant TOML.TOML_Value :=
      TOML.File_IO.Load_File ("example.toml").Value;
begin
   for E of Value.Iterate_On_Table loop
      TIO.Put_Line (Ada.Strings.Unbounded.To_String (E.Key) & " is a "
                    & E.Value.Kind'Image);
   end loop;
end Test_Iterate_on_Table;
