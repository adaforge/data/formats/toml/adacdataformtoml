--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: BSD-3-Clause
--  SPDX-Creator: Pierre-Marie de Rodat (derodat@adacore.com)
--  SPDX-FileCopyrightText: Copyright 2019 AdaCore (derodat@adacore.com)
--  SPDX-FileContributor: 2019 - 2022 Pierre-Marie de Rodat (pmderodat@kawie.fr)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2019-03-25
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------
pragma License (Unrestricted);

with Ada.Text_IO;

with TOML;
with TOML.File_IO;

procedure Test_Clone is
   Value : constant TOML.TOML_Value :=
      TOML.File_IO.Load_File ("example.toml").Value;
   Clone : constant TOML.TOML_Value := Value.Clone;
begin
   --  Mutate Value: the Clone is supposed to be unaffected

   Value.Set ("hello", TOML.Create_Boolean (true));
   Value.Get ("array").Append (TOML.Create_Boolean (false));

   Ada.Text_IO.Put_Line (Clone.Dump_As_String);
end Test_Clone;
